package com.mygdx.game;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Tools.ScreenManager;
import com.mygdx.game.components.components.CircleComponent;

import com.mygdx.game.components.components.PositionComponent;

import com.mygdx.game.components.components.VelocityComponent;

import java.util.Random;

public class AgarIOGame extends Game {


    private TextureAtlas atlas;
    private ShapeRenderer renderer;
    private Stage stage;
    private Viewport viewport;
    private com.mygdx.game.Tools.ScreenManager screenManager;
    private PooledEngine gameEngine;
    public boolean isGameEnded;
    private Camera camera;
    private Random random;

    public AssetManager getAssetManager() {
        return assetManager;
    }

    private AssetManager assetManager;

    @Override
    public void create() {
        assetManager = new AssetManager();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.x = Gdx.graphics.getWidth() / 2;
        camera.position.y = Gdx.graphics.getHeight() / 2;
        camera.update();
        random = new Random();
        renderer = new ShapeRenderer();
        renderer.setAutoShapeType(true);
        gameEngine = new PooledEngine();
        viewport = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        atlas = new TextureAtlas(Gdx.files.internal("pack"));
        stage = new Stage(viewport);
        screenManager = com.mygdx.game.Tools.ScreenManager.getScreenManager(this);
        //synchronizing asset loading
        assetManager.load("music.mp3", Music.class);
        assetManager.load("sound.ogg", Sound.class);
        assetManager.finishLoading();
        this.setScreen(screenManager.getMenuScreen());


    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public Stage getStage() {
        return stage;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public ScreenManager getScreenManager() {
        return screenManager;
    }

    public TextureAtlas getAtlas() {
        return atlas;
    }

    public PooledEngine getGameEngine() {
        return gameEngine;
    }

    public ShapeRenderer getRenderer() {
        return renderer;
    }

    public void addComponentsToEntity(Entity entity, Component... components) {
        for (Component component : components) {
            entity.add(component);
        }
    }

    public Camera getCamera() {
        return camera;
    }

    public float calculateDistanceBetweenPoints(float x2, float y2, float x1, float y1) {
        return (float) Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

    public int getRandomNumberInRange(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }


}
