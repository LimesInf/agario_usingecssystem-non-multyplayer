package com.mygdx.game.Tools;


import com.badlogic.ashley.core.EntitySystem;
import com.mygdx.game.AgarIOGame;
import com.mygdx.game.systems.AiSystem;
import com.mygdx.game.systems.BotGeneratorSystem;
import com.mygdx.game.systems.CollisionSystem;
import com.mygdx.game.systems.DrawSystem;
import com.mygdx.game.systems.PlayerControllSystem;

import java.util.ArrayList;

public class GameSystemUtils {
    private DrawSystem drawSystem;
    private AiSystem aiSystem;
    private PlayerControllSystem playerControllSystem;
    private CollisionSystem collisionSystem;
    private BotGeneratorSystem generatorSystem;
    private ArrayList<EntitySystem> systems;
    public GameSystemUtils(AgarIOGame agarIOGame) {
        systems=new ArrayList<EntitySystem>();

        generatorSystem=new BotGeneratorSystem(agarIOGame);
        playerControllSystem = new PlayerControllSystem(agarIOGame);
        aiSystem = new AiSystem(agarIOGame);
        drawSystem = new DrawSystem(agarIOGame, agarIOGame.getRenderer());
        collisionSystem=new CollisionSystem(agarIOGame,playerControllSystem);

        systems.add(generatorSystem);
        systems.add(aiSystem);
        systems.add(playerControllSystem);
        systems.add(drawSystem);
        systems.add(collisionSystem);


    }

    public ArrayList<EntitySystem> getSystems() {
        return systems;
    }
}
