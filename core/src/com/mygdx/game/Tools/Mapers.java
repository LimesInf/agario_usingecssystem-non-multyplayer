package com.mygdx.game.Tools;

import com.badlogic.ashley.core.ComponentMapper;
import com.mygdx.game.components.components.CircleComponent;
import com.mygdx.game.components.components.PositionComponent;
import com.mygdx.game.components.components.VelocityComponent;

public  class Mapers {
  public static final ComponentMapper<PositionComponent> pm =ComponentMapper.getFor(PositionComponent.class);
  public static final ComponentMapper<CircleComponent> clm =ComponentMapper.getFor(CircleComponent.class);
  public static final ComponentMapper<VelocityComponent> vm =ComponentMapper.getFor(VelocityComponent.class);
}
