package com.mygdx.game.components.components;

import com.badlogic.ashley.core.Component;

public class PositionComponent implements Component {
    public float posx;
    public float posy;
}
