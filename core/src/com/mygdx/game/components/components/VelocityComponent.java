package com.mygdx.game.components.components;

import com.badlogic.ashley.core.Component;

public class VelocityComponent implements Component {
    public float velocity_x;
    public float velocity_y;
}
