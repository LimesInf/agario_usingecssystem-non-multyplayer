package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.game.AgarIOGame;

public abstract class DefAbstractScreen implements Screen {
    protected Table table;
    protected AgarIOGame game;
    protected Label.LabelStyle style;

    public DefAbstractScreen(AgarIOGame agarIOGame) {
        game = agarIOGame;
        style = new Label.LabelStyle(new BitmapFont(), Color.CYAN);
        init();
        Gdx.input.setInputProcessor(game.getStage());
    }

    private void init() {
        game.getStage().clear();
        table = new Table();
        table.setFillParent(true);
        game.getStage().addActor(table);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        game.getStage().draw();
    }

    @Override
    public void resize(int width, int height) {
        game.getViewport().update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        game.getStage().dispose();
    }
}
