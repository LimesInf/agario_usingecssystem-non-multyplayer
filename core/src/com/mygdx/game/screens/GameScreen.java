package com.mygdx.game.screens;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.mygdx.game.AgarIOGame;
import com.mygdx.game.Tools.GameSystemUtils;

public class GameScreen extends DefAbstractScreen {
    private Label score;
    private Label level;

    private GameSystemUtils gameSystemUtils;

    public GameScreen(AgarIOGame agarIOGame) {
        super(agarIOGame);
        game.isGameEnded = false;
        //Systems
        clearEntities();
        gameSystemUtils = new GameSystemUtils(game);
        initSystems();

    }

    @Override
    public void render(float delta) {
        if (game.isGameEnded) {
            game.setScreen(game.getScreenManager().getMenuScreen());
        } else {
            super.render(delta);
            Gdx.gl.glClearColor(1, 1, 1, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
            game.getStage().draw();
            game.getGameEngine().update(delta);
        }

    }


    @Override
    public void pause() {
        super.pause();

    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {

    }

    private void initSystems() {
        for (EntitySystem entitySystemL : gameSystemUtils.getSystems()) {
            game.getGameEngine().addSystem(entitySystemL);
        }
    }

    private void clearEntities() {
        game.getGameEngine().removeAllEntities();
    }
}
