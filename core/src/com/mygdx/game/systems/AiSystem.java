package com.mygdx.game.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.AgarIOGame;
import com.mygdx.game.Tools.Mapers;
import com.mygdx.game.components.components.CircleComponent;
import com.mygdx.game.components.components.PositionComponent;
import com.mygdx.game.components.components.VelocityComponent;

import java.util.ArrayList;

public class AiSystem extends EntitySystem {
    private static final int heightBound = Gdx.graphics.getHeight();
    private static final int widthBound = Gdx.graphics.getHeight();
    private Vector2 randomPosition;
    private float timer;
    private ImmutableArray<Entity> movableEntities;
    private ArrayList<Vector2> entitiesDestinations;
    private AgarIOGame game;
    private MODELSTATE modelstate;

    public AiSystem(AgarIOGame game) {
        modelstate=MODELSTATE.random;
        this.game=game;
        movableEntities = game.getGameEngine().getEntitiesFor(Family.all(CircleComponent.class, PositionComponent.class, VelocityComponent.class).get());
        timer = 0;
        entitiesDestinations = new ArrayList<Vector2>();
        genDestinations();
    }

    @Override
    public void update(float deltaTime) {
        timer += deltaTime;
        if (timer >= 3) {
            genDestinations();
            timer = 0;
        }


        for (int i = 0; i < movableEntities.size(); i++) {
            if (entitiesDestinations.size() != 0) {
                Entity entity = movableEntities.get(i);
                Vector2 randomPos = entitiesDestinations.get(i);
                moveEntity(randomPos, entity, deltaTime);
            }
        }
    }

    private Vector2 pickPositionInRange(float buttom,float top) {
        return new Vector2(buttom,top).nor();
    }



    private void genDestinations() {
        entitiesDestinations.clear();
        for (int i = 0; i <= movableEntities.size(); i++) {
            if(modelstate==MODELSTATE.random) {
                randomPosition = pickPositionInRange(game.getRandomNumberInRange(-widthBound + 100, widthBound - 100), game.getRandomNumberInRange(-heightBound + 100, heightBound - 100));
            }
            entitiesDestinations.add(randomPosition);
        }
    }

    private void moveEntity(Vector2 destionation, Entity entity, float deltaTime) {
        VelocityComponent velocityComponent = Mapers.vm.get(entity);
        PositionComponent positionComponent = Mapers.pm.get(entity);
        //world is now infinity do not need handle
        positionComponent.posx += destionation.x * velocityComponent.velocity_x * deltaTime;
        positionComponent.posy += destionation.y * velocityComponent.velocity_y * deltaTime;

    }
    // karoxa chhascnem :D  minchem verj implement anem :D :D :D :D :D
    private enum MODELSTATE{
       murder,random
    }
}
