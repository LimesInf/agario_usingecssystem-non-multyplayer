package com.mygdx.game.systems;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.mygdx.game.AgarIOGame;
import com.mygdx.game.components.components.CircleComponent;
import com.mygdx.game.components.components.PositionComponent;
import com.mygdx.game.components.components.VelocityComponent;

import java.util.Random;

public class BotGeneratorSystem extends EntitySystem {
    private static final int boundX=Gdx.graphics.getWidth();
    private static final int boundY=Gdx.graphics.getHeight();
    private static final int startupMaximumRadius = 30;
    private static Color[] randomColors = new Color[]{
            Color.BLUE, Color.FIREBRICK, Color.ORANGE, Color.LIME, Color.DARK_GRAY, Color.NAVY, Color.GOLD, Color.PINK
    };

    private int dotCount = 1000;
    private int aiCount = 30;
    private Random random;
    private AgarIOGame game;
    private float generatorTimer;
    private int generatCount;
    private int staticMemberColorIndex;


    public BotGeneratorSystem(AgarIOGame game) {
        this.game = game;
        generatCount = 0;
        random = new Random();
        staticMemberColorIndex = random.nextInt(randomColors.length);
        initStatics();
        initAi();
    }

    @Override
    public void update(float deltaTime) {
        generatorTimer += deltaTime;
        if (generatorTimer >= 4 && generatCount <= 30) {
            generatorTimer = 0;
            generatCount++;
            dotCount = random.nextInt(200);
            initStatics();
        }
    }

    private void initStatics() {
        for (int i = 0; i < dotCount; i++) {

            Entity entity = game.getGameEngine().createEntity();
            PositionComponent positionComponent = game.getGameEngine().createComponent(PositionComponent.class);
            CircleComponent circleComponent = game.getGameEngine().createComponent(CircleComponent.class);

            circleComponent.radiusOfCircle = random.nextInt(startupMaximumRadius);
            circleComponent.color = randomColors[staticMemberColorIndex];

            positionComponent.posx = (game.getRandomNumberInRange(-boundX*4,boundX*4));
            positionComponent.posy = (game.getRandomNumberInRange(-boundY*4,boundY*4));

            game.addComponentsToEntity(entity, positionComponent, circleComponent);
            game.getGameEngine().addEntity(entity);
        }
    }


    private void initAi() {
        for (int i = 0; i < aiCount; i++) {
            Entity entity = game.getGameEngine().createEntity();
            PositionComponent positionComponent = game.getGameEngine().createComponent(PositionComponent.class);
            CircleComponent circleComponent = game.getGameEngine().createComponent(CircleComponent.class);
            VelocityComponent velocityComponent = game.getGameEngine().createComponent(VelocityComponent.class);

            positionComponent.posx = (game.getRandomNumberInRange(-boundX*3,boundX*3));
            positionComponent.posy = (game.getRandomNumberInRange(-boundY*3,boundY*3));

            circleComponent.color = randomColors[random.nextInt(randomColors.length)];
            circleComponent.radiusOfCircle = 50 + random.nextInt(50);

            velocityComponent.velocity_x = 80 + (float) (Math.random() * 8);
            velocityComponent.velocity_y = 80 + (float) (Math.random() * 8);

            game.addComponentsToEntity(entity, circleComponent, velocityComponent, positionComponent);
            game.getGameEngine().addEntity(entity);
        }
    }
}
