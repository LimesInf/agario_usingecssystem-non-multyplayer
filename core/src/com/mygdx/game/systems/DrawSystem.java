package com.mygdx.game.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.AgarIOGame;
import com.mygdx.game.Tools.Mapers;
import com.mygdx.game.components.components.CircleComponent;

import com.mygdx.game.components.components.PositionComponent;

public class DrawSystem extends EntitySystem {
    private ShapeRenderer shapeRenderer;
    private ImmutableArray<Entity> entities;
   private AgarIOGame game;
    public DrawSystem(AgarIOGame game, ShapeRenderer shapeRenderer){
        this.game=game;
        entities=game.getGameEngine().getEntities();
        this.shapeRenderer=shapeRenderer;
    }
    @Override
    public void update(float deltaTime) {
        shapeRenderer.begin();
        shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setProjectionMatrix(game.getCamera().combined);
        for(Entity entity:entities){
            PositionComponent positionComponent= Mapers.pm.get(entity);
            CircleComponent circleComponent=Mapers.clm.get(entity);
            shapeRenderer.setColor(circleComponent.color);
            shapeRenderer.circle(positionComponent.posx,positionComponent.posy,circleComponent.radiusOfCircle);
        }
        shapeRenderer.end();
    }
}
