package com.mygdx.game.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.mygdx.game.AgarIOGame;
import com.mygdx.game.components.components.CircleComponent;
import com.mygdx.game.components.components.PositionComponent;
import com.mygdx.game.components.components.VelocityComponent;

public class PlayerControllSystem extends EntitySystem {
    private AgarIOGame game;
    private Entity player;
    private PositionComponent positionComponent;
    private CircleComponent circleComponent;
    private float destX;
    private float destY;
    private Vector2 staticTargetVector;
    private float velocity;



    public PlayerControllSystem(final AgarIOGame game) {
        this.game = game;
        createUser();
        velocity=400;
        game.getStage().addListener(new DragListener(){
            @Override
            public boolean mouseMoved(InputEvent event, float x, float y) {
                destX=x;
                destY=y;
                return true;
            }

            @Override
            public void drag(InputEvent event, float x, float y, int pointer) {
                super.drag(event, x, y, pointer);
                destY=y;
                destX=x;
            }
        });
    }

    private void createUser() {
        staticTargetVector =new Vector2();
        player = game.getGameEngine().createEntity();
        positionComponent = game.getGameEngine().createComponent(PositionComponent.class);
        circleComponent = game.getGameEngine().createComponent(CircleComponent.class);
        positionComponent.posx = Gdx.graphics.getWidth() / 2;
        positionComponent.posy = Gdx.graphics.getHeight() / 2;
        circleComponent.radiusOfCircle = 35;
        circleComponent.color = Color.BLACK;
        game.addComponentsToEntity(player, positionComponent, circleComponent, new VelocityComponent());
        game.getGameEngine().addEntity(player);
    }
    @Override
    public void update(float deltaTime) {
        staticTargetVector.set(destX-Gdx.graphics.getWidth()/2,destY-Gdx.graphics.getHeight()/2);
        staticTargetVector.nor();
        positionComponent.posx += deltaTime * staticTargetVector.x*velocity;
        positionComponent.posy += deltaTime * staticTargetVector.y*velocity;
        game.getCamera().position.x=positionComponent.posx;
        game.getCamera().position.y=positionComponent.posy;
        game.getCamera().update();
    }
    public Entity getPlayer() {
        return player;
    }
}